package com.chromstudio.barebone.common.enums;

public class ControllerMappingEnum {


    private ControllerMappingEnum(){
        throw new IllegalStateException ("Static class utility");
    }


    public static final String PREFIX = "/api";
    public static final String USER = "/user";
    public static final String ADMIN = "/admin";
    public static final String ACTIVATE_ACCOUNT = "/activate-account";
    public static final String REGISTER = "/register";

//    ADMIN("/admin"),
//    USER("/user"),
//    REGISTER( "/register");
//
//    public final String value;
//
//    private ControllerMappingEnum(String value) {
//        this.value = value;
//    }
//
//    @Override public String toString() {
//        return this.value;
//    }
}