package com.chromstudio.barebone.common.utils;

import com.chromstudio.barebone.common.exceptions.NoSuchElementFoundException;
import com.chromstudio.barebone.model.response.ErrorResponse;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.Objects;

@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

    public static final String TRACE = "trace";

    @Value("${reflectoring.trace:false}")
    private boolean printStackTrace;

    @Override
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    protected ResponseEntity <Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        return super.handleMethodArgumentNotValid ( ex, headers, status, request );
    }

    @ExceptionHandler(NoSuchElementFoundException.class)
    @ResponseStatus(HttpStatus.I_AM_A_TEAPOT)
    public ResponseEntity<Object> handleNoSuchElementFoundException(NoSuchElementFoundException exception, WebRequest request){
        logger.error ("Failed to find the requested element", exception);
        return buildErrorResponse ( exception, HttpStatus.I_AM_A_TEAPOT, request );
    }

    private ResponseEntity<Object> buildErrorResponse(Exception exception,
                                                      HttpStatus httpStatus,
                                                      WebRequest request){
        return buildErrorResponse ( exception, exception.getMessage (), httpStatus, request );
    }

    private ResponseEntity<Object> buildErrorResponse(Exception exception,
                                                      String message,
                                                      HttpStatus httpStatus,
                                                      WebRequest request){
        ErrorResponse errorResponse = new ErrorResponse ( httpStatus.value (), message );
        if(printStackTrace && isTraceOn(request)){
            errorResponse.setStacktrace ( ExceptionUtils.getStackTrace ( exception ) );
        }

        return ResponseEntity.status ( httpStatus.value () ).body ( errorResponse );
    }

    private boolean isTraceOn(WebRequest request){
        String[] value = request.getParameterValues ( TRACE );
        return Objects.nonNull (value)
                && value.length > 0
                && value[0].contentEquals ( "true" );
    }

    @Override
    protected ResponseEntity <Object> handleExceptionInternal(Exception ex, Object body, HttpHeaders headers, HttpStatus status, WebRequest request) {
        return buildErrorResponse ( ex, status, request );
    }
}