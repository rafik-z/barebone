package com.chromstudio.barebone.service;

import com.chromstudio.barebone.dto.UserRegistrationDto;
import com.chromstudio.barebone.model.Role;
import com.chromstudio.barebone.model.request.UserInformationRequest;
import com.chromstudio.barebone.repository.RoleRepository;
import com.chromstudio.barebone.repository.UserRepository;
import com.chromstudio.barebone.model.User;
import com.chromstudio.barebone.model.auth.UserDetailsImpl;
import javassist.tools.web.BadHttpRequest;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.InvalidPropertyException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.io.InvalidObjectException;
import java.util.*;

@Service
public class UserService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        User user = userRepository.findByEmail (email).orElse ( null);
        if (Objects.isNull (user)){
            throw new UsernameNotFoundException ( email + " not found." );
        }
        return new UserDetailsImpl ( user );
    }

    public void registerUser(UserRegistrationDto userDto) throws InvalidObjectException {
        String valid = this.userRegistrationDtoValidator ( userDto );
        if(StringUtils.isNotBlank (valid) ){
            throw new InvalidObjectException ( valid );
        }
        List <Role> roles = roleRepository.findAll ();
        if(CollectionUtils.isEmpty ( roles )){

        }
        User user = new User ();
        user.setEmail ( userDto.getEmail () );
        user.setPassword (passwordEncoder.encode ( userDto.getPassword () ));
        Set <Role> rolesToAdd = new HashSet <> ();
        rolesToAdd.add ( roles.get (0) );
        user.setRoles ( rolesToAdd );
        user.setEnabled ( true );
        User savedUser = userRepository.save ( user );
        System.out.println (savedUser.toString ());
        System.out.println (savedUser.getRoles ().stream().findFirst ().get ().getName ().toString ());
    }

    public User getUserInformation(UserInformationRequest request) throws BadHttpRequest {
        User userToReturn = null;

        if(Objects.isNull ( request.getUserId () ) || request.getUserId ()  <= 0){
            throw new BadHttpRequest ();
        };

        userToReturn = userRepository.findById ( request.getUserId () ).orElse ( null );


        return userToReturn;
    }

    private String userRegistrationDtoValidator(UserRegistrationDto userRegistrationDto){
        List<String> missingEntries = new ArrayList <> ();

        if(StringUtils.isEmpty ( userRegistrationDto.getEmail ())){
            missingEntries.add ( "email, " );
        }

        if(StringUtils.isEmpty ( userRegistrationDto.getPassword () )){
            missingEntries.add ( "password, " );
        }

        if(StringUtils.isEmpty ( userRegistrationDto.getMatchingPassword () )){
            missingEntries.add ( "matching password." );
        }

        if(missingEntries.size () > 0) {
            StringBuilder builder = new StringBuilder ("Missing entries : ");
            missingEntries.forEach ( entry -> {
                builder.append ( entry );
            } );
            return builder.toString ();
        }

        return null;

    }
}