package com.chromstudio.barebone.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import javax.mail.internet.MimeMessage;

@Service
public class EmailServiceImpl implements EmailService{

    @Autowired
    private JavaMailSender mailSender;

    @Override public void sendSimpleMessage(String to, String subject, String text) {

        try {
            SimpleMailMessage message = new SimpleMailMessage ();
            message.setFrom ( "noreply@zefy.fr" );
            message.setTo ( to );
            message.setSubject ( subject );
            message.setText ( text );

            mailSender.send ( message );
        } catch (MailException e) {
            e.printStackTrace ();
        }


    }
}