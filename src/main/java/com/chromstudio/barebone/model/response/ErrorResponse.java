package com.chromstudio.barebone.model.response;

import com.fasterxml.jackson.annotation.JsonIgnoreType;
import org.springframework.boot.context.properties.bind.validation.ValidationErrors;
import org.springframework.http.HttpStatus;
import org.springframework.web.context.request.WebRequest;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ErrorResponse {

    public final int status;
    public final String message;

    private String stacktrace;
    private List <ValidationError> errors;


    public ErrorResponse(int status, String message ) {
        this.status = status;
        this.message = message;
    }

    public ErrorResponse(Exception exception, String message, int httpStatus, WebRequest request) {
        this.status = httpStatus;
        this.message = message;
    }

    private static class ValidationError {

        private final String field;
        private final String message;

        public ValidationError(String field, String message) {
            this.field = field;
            this.message = message;
        }

        public String getField() {
            return field;
        }

        public String getMessage() {
            return message;
        }
    }

    public int getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public String getStacktrace() {
        return stacktrace;
    }

    public void setStacktrace(String stacktrace) {
        this.stacktrace = stacktrace;
    }

    public List <ValidationError> getErrors() {
        return errors;
    }

    public void setErrors(List <ValidationError> errors) {
        this.errors = errors;
    }

    public void addValidationError(String field, String message){
        if(Objects.isNull (errors)){
            errors = new ArrayList <> ();
        }

        errors.add ( new ValidationError ( field, message ) );
    }
}