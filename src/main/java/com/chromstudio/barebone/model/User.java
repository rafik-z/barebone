package com.chromstudio.barebone.model;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

import static javax.persistence.GenerationType.SEQUENCE;

@Table(name = "users",
        uniqueConstraints = {
                @UniqueConstraint(
                        name = "user_email_unique",
                        columnNames = "email"
                )
        })
@Entity(name = "user")
public class User {

    @Id
    @SequenceGenerator(
            name = "user_sequence",
            sequenceName = "user_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = SEQUENCE,
            generator = "user_sequence"
    )
    @Column(
            name = "user_id",
            updatable = false,
            unique = true
    )
    private Long id;

    @Column(
            name = "email",
            nullable = false,
            columnDefinition = "TEXT")
    private String email;

    @Column(
            name = "password",
            nullable = false,
            columnDefinition = "TEXT")
    private String password;

    @Column(
            name = "enabled",
            nullable = false,
            columnDefinition = "BOOLEAN")
    private Boolean isEnabled;

    public User() {
        this.isEnabled = true;
    }

    public User(Long id, String password, String email, Set <Role> roles, Boolean isEnabled) {
        this.id = id;
        this.password = password;
        this.email = email;
        this.roles = roles;
        this.isEnabled = isEnabled;
    }

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(
            name = "users_roles",
            joinColumns = @JoinColumn(
                    name = "user_id"
            ),
            inverseJoinColumns = @JoinColumn(
                    name = "role_id"
            )
    )
    private Set <Role> roles = new HashSet <> ();

    public Long getId() {
        return id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Set <Role> getRoles() {
        return roles;
    }

    public void setRoles(Set <Role> roles) {
        this.roles = roles;
    }

    public Boolean isEnabled() {
        return isEnabled;
    }

    public void setEnabled(Boolean enabled) {
        isEnabled = enabled;
    }

    @Override public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass () != o.getClass ()) return false;

        User user = (User) o;

        return new EqualsBuilder ().append ( id, user.id ).append ( email, user.email ).append ( roles, user.roles ).isEquals ();
    }

    @Override public int hashCode() {
        return new HashCodeBuilder ( 17, 37 ).append ( id ).append ( email ).append ( roles ).toHashCode ();
    }

    @Override public String toString() {
        return "User{" +
                "id=" + id +
                ", email='" + email + '\'' +
                ", roles=" + roles +
                '}';
    }
}