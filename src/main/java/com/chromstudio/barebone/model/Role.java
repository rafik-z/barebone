package com.chromstudio.barebone.model;


import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import javax.persistence.*;

@Entity
@Table(name = "roles",
        uniqueConstraints = {
                @UniqueConstraint(name = "role_name_unique",
                    columnNames = "name")
        }
)
public class Role implements GrantedAuthority {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(
            name = "role_id",
            updatable = false
    )
    private Integer id;

    @Column(name = "name",
            nullable = false,
            updatable = false,
            unique = true,
            columnDefinition = "TEXT")
    private String name;

    public Role() {
    }

    public Role(String role) {
        this.name = role;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override public String getAuthority() {
        return name;
    }

    @Override public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass () != o.getClass ()) return false;

        Role role = (Role) o;

        return new EqualsBuilder ().append ( id, role.id ).append ( name, role.name ).isEquals ();
    }

    @Override public int hashCode() {
        return new HashCodeBuilder ( 17, 37 ).append ( id ).append ( name ).toHashCode ();
    }

    @Override public String toString() {
        return "Role{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}