package com.chromstudio.barebone.model.request;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class UserInformationRequest {

    private Long userId;
    private boolean self = true;

    public UserInformationRequest(){

    }

    public UserInformationRequest(Long userId, boolean self) {
        this.userId = userId;
        this.self = self;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public boolean isSelf() {
        return self;
    }

    public void setSelf(boolean self) {
        this.self = self;
    }

    @Override public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass () != o.getClass ()) return false;

        UserInformationRequest that = (UserInformationRequest) o;

        return new EqualsBuilder ().append ( self, that.self ).append ( userId, that.userId ).isEquals ();
    }

    @Override public int hashCode() {
        return new HashCodeBuilder ( 17, 37 ).append ( userId ).append ( self ).toHashCode ();
    }

    @Override public String toString() {
        return "UserInformationRequest{" +
                "userId='" + userId + '\'' +
                ", self=" + self +
                '}';
    }
}