package com.chromstudio.barebone.security;

import com.chromstudio.barebone.security.config.JwtConfig;
import com.google.common.base.Strings;
import io.jsonwebtoken.*;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.crypto.SecretKey;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class JwtTokenVerifier extends OncePerRequestFilter {

    private final SecretKey secretKey;
    private final JwtConfig jwtConfig;

    public JwtTokenVerifier(SecretKey secretKey, JwtConfig jwtConfig) {
        this.secretKey = secretKey;
        this.jwtConfig = jwtConfig;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {

        String authorizationHeader = request.getHeader ( jwtConfig.getAuthorizationHeader () );

        if (Strings.isNullOrEmpty ( authorizationHeader ) || !authorizationHeader.startsWith ( jwtConfig.getTokenPrefix () )) {
            filterChain.doFilter ( request, response );
            return;
        }

        String token = authorizationHeader.replace ( jwtConfig.getTokenPrefix (), "" );

        try {
            JwtParser parser = Jwts.parserBuilder ()
                    .setSigningKey ( secretKey )
                    .build ();
            Jws <Claims> claimsJws = parser.parseClaimsJws ( token );

            Claims body = claimsJws.getBody ();

            String username = body.getSubject ();
            List<Map <String, String>> authorities = (List <Map <String, String>>) body.get("authorities");
            Set <SimpleGrantedAuthority> grantedAuthorities = authorities.stream()
                    .map ( entry -> new SimpleGrantedAuthority ( entry.get ( "authority" ) ))
                    .collect ( Collectors.toSet () );

            Authentication authentication = new UsernamePasswordAuthenticationToken ( username, null, grantedAuthorities );

            SecurityContextHolder.getContext ().setAuthentication ( authentication );

            filterChain.doFilter ( request, response );
        } catch (JwtException e){
            throw new IllegalStateException (String.format("Token %s can not be trusted.", token));
        }

    }
}