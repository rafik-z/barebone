package com.chromstudio.barebone.security.config;

import com.chromstudio.barebone.security.JwtTokenVerifier;
import com.chromstudio.barebone.security.config.JwtConfig;
import com.chromstudio.barebone.service.UserService;
import com.chromstudio.barebone.model.auth.JwtUsernameAndPasswordAuthenticationFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.crypto.SecretKey;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class ApplicationSecurityConfig extends WebSecurityConfigurerAdapter {

    private final PasswordEncoder passwordEncoder;
    private final UserService userService;

    private final SecretKey secretKey;
    private final JwtConfig jwtConfig;

    @Autowired
    public ApplicationSecurityConfig(PasswordEncoder passwordEncoder,
                                     UserService userService, SecretKey secretKey, JwtConfig jwtConfig) {
        this.passwordEncoder = passwordEncoder;
        this.userService = userService;
        this.secretKey = secretKey;
        this.jwtConfig = jwtConfig;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .csrf()
                    .disable()
                .sessionManagement ()
                    .sessionCreationPolicy ( SessionCreationPolicy.STATELESS )
                .and ()
                .addFilterAfter ( new JwtTokenVerifier ( secretKey, jwtConfig ), JwtUsernameAndPasswordAuthenticationFilter.class )
                .addFilter ( new JwtUsernameAndPasswordAuthenticationFilter ( authenticationManager (), jwtConfig, secretKey ) )
                .authorizeRequests()
                .antMatchers("/","/mail","/register", "/user/**").permitAll()
                .antMatchers("/api/**").hasAnyAuthority ( "ADMIN", "USER" )
                .anyRequest()
                .authenticated()
        ;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(daoAuthenticationProvider());
    }

    @Bean
    public DaoAuthenticationProvider daoAuthenticationProvider() {
        DaoAuthenticationProvider provider = new DaoAuthenticationProvider();
        provider.setPasswordEncoder(passwordEncoder);
        provider.setUserDetailsService( userService );
        return provider;
    }


}

//@Configuration
//@EnableWebSecurity
//@EnableGlobalMethodSecurity(prePostEnabled = true)
//public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
//
//    private final PasswordEncoder passwordEncoder;
//    private final UserDetailsServiceImpl userDetailsService;
//
//    private final SecretKey secretKey;
//    private final JwtConfig jwtConfig;
//
//    @Autowired
//    public WebSecurityConfig(PasswordEncoder passwordEncoder, UserDetailsServiceImpl userDetailsService, SecretKey secretKey, JwtConfig jwtConfig) {
//        this.passwordEncoder = passwordEncoder;
//        this.userDetailsService = userDetailsService;
//        this.secretKey = secretKey;
//        this.jwtConfig = jwtConfig;
//    }
//
//    @Bean
//    public DaoAuthenticationProvider daoAuthenticationProvider(){
//        DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider ();
//        authProvider.setUserDetailsService ( userDetailsService );
//        authProvider.setPasswordEncoder ( passwordEncoder );
//
//        return authProvider;
//    }
//
//    @Override protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//        auth.authenticationProvider ( daoAuthenticationProvider () );
//    }
//
//    @Override
//    protected void configure(HttpSecurity http) throws Exception{
//        http.csrf ().disable ()
//                .sessionManagement ()
//                    .sessionCreationPolicy ( SessionCreationPolicy.STATELESS )
//                .and ()
//                .addFilter ( new JwtUsernameAndPasswordAuthenticationFilter ( authenticationManager (), jwtConfig, secretKey ) )
//                .addFilterAfter ( new JwtTokenVerifier ( secretKey, jwtConfig ), JwtUsernameAndPasswordAuthenticationFilter.class )
//                .authorizeRequests ()
//                .antMatchers ( "/api/user/register" ).permitAll ()
//                .antMatchers ( "/api/**" ).hasAnyRole ( "USER","ADMIN" )
//                .anyRequest ()
//                .authenticated ();
////        http.authorizeRequests ()
////                .antMatchers ( "/api/user/register" ).permitAll ()
////                .antMatchers ( "/api/**" ).hasAnyAuthority ( "USER", "ADMIN" )
////                .anyRequest ().authenticated ();
//    }
//}