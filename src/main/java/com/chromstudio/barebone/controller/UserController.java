package com.chromstudio.barebone.controller;

import com.chromstudio.barebone.common.enums.ControllerMappingEnum;
import com.chromstudio.barebone.common.exceptions.NoSuchElementFoundException;
import com.chromstudio.barebone.dto.UserRegistrationDto;
import com.chromstudio.barebone.model.User;
import com.chromstudio.barebone.model.request.UserInformationRequest;
import com.chromstudio.barebone.service.UserService;
import javassist.tools.web.BadHttpRequest;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Nonnull;
import javax.servlet.http.HttpServletRequest;
import java.io.InvalidObjectException;
import java.util.Objects;

@RestController()
@RequestMapping(value = ControllerMappingEnum.USER)
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping(value = ControllerMappingEnum.REGISTER)
    public void registerUser(@RequestBody @Nonnull UserRegistrationDto userToRegister) throws InvalidObjectException {
        if(Objects.isNull ( userToRegister )){
            throw new InvalidObjectException ( "Null request" );
        }
        userService.registerUser ( userToRegister );
    }

    @GetMapping(value = ControllerMappingEnum.ACTIVATE_ACCOUNT + "/{hash}")
    public void activateRegisteredAccount(HttpServletRequest request, @PathVariable("hash") String hash) throws NoSuchElementFoundException {
        if(StringUtils.isNotEmpty ( hash )){
            throw new NoSuchElementFoundException ();
        }
        System.out.println (request.getServletPath ());

    }

    @GetMapping()
    @PreAuthorize ( "hasAnyAuthority('ADMIN','USER')" )
    public User getUserInformation(HttpServletRequest httpRequest, @RequestBody UserInformationRequest request) throws BadHttpRequest {
       return userService.getUserInformation(request);
    }
}