package com.chromstudio.barebone.controller;

import com.chromstudio.barebone.service.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/mail")
public class MailController {

    @Autowired
    private EmailService emailService;

    @GetMapping()
    public String sender(){
        emailService.sendSimpleMessage ( "wassim.zebdi@gmail.com", "Test Zefy", "COUCOU!!!" );
        return "ok";
    }

}