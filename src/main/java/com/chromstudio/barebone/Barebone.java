package com.chromstudio.barebone;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Barebone {

	public static void main(String[] args) {
		SpringApplication.run(Barebone.class, args);
	}

}